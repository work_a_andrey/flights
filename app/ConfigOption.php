<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigOption extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_options';

    public $timestamps = false;

    protected $primaryKey = 'name';


    /**
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public static function get($key)
    {
        if(!$option = ConfigOption::where('name', '=', $key)->first()) {
            return null;
        }

        $value = json_decode($option->value, true);
        return json_last_error() == JSON_ERROR_NONE ? $value : $option->value;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public static function set($key, $value)
    {
        if(is_array($value)) {
            $value = json_encode($value);
        }
        if(!$option = ConfigOption::where('name', '=', $key)->first()) {
            $option = new ConfigOption();
            $option->name = $key;
        }
        $option->value = $value;
        return $option->save();
    }

    /**
     * @param string $prefix
     * @return string
     */
    public function shortName($prefix)
    {
        return str_replace($prefix . '.', '', $this->name);
    }
}
