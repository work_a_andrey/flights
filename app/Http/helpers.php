<?php

/**
 * @return \App\User
 */
function logged_user()
{
    return Auth::user();
}

/**
 * @param string $url
 * @return bool
 */
function is_current_url($url) {
    return $_SERVER['REQUEST_URI'] == $url;
}

function manage_object_images($object)
{
    $view = view('helpers.manage_object_assets')->with([
        'object' => $object
    ]);
   echo $view;
}