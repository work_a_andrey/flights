<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('templates.index');
});

Route::get('/asset/{id}', ['uses' => 'AssetController@view']);

Route::group(['middleware' => ['guest']], function()
{
    Route::get('admin/login', function() {

        return view('templates.admin.login');
    });
    Route::post('/admin/login', ['uses' => 'AdminLoginController@index']);
});

Route::group(['middleware' => ['App\Http\Middleware\Admin']], function()
{
    Route::get('/admin', ['uses' => 'AdminController@index']);
    Route::get('/asset/{id}/delete', ['uses' => 'AssetController@delete']);

    Route::resource('/admin/flights', 'AdminFlightController');
});