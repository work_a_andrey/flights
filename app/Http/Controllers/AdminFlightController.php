<?php

namespace App\Http\Controllers;

use App\Model\Aircraft;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\Flight;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class AdminFlightController extends Controller
{
    /**
     * @return $this
     */
    public function index()
    {
        return view('templates.admin.flights.index')->with([
            'flights' => Flight::paginate(30),
        ]);
    }

    /**
     * @return $this
     */
    public function create()
    {
        $flight = new Flight();
        return view('templates.admin.flights.create')->with([
            'flight' => $flight
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $data = Input::get('flight');
        if($aircraft = Aircraft::find($data['aircraftId'])) {
            $data['airlineId'] = $aircraft->airlineId;
        }
        $flight = Flight::create($data);
        return redirect('/admin/flights/' . $flight->id)
            ->with('status', 'Flight was successfully created');
    }

    /**
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $flight = Flight::findOrFail($id);
        return view('templates.admin.flights.edit')->with([
            'flight' => $flight
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id)
    {
        $flight = Flight::findOrFail($id);
        $data = Input::get('flight');
        if($aircraft = Aircraft::find($data['aircraftId'])) {
            $data['airlineId'] = $aircraft->airlineId;
        }
        $flight->update($data);
        return redirect('/admin/flights/' . $flight->id)
            ->with('status', 'Flight was successfully updated');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        return redirect('/admin/flights/' . $id . '/edit');
    }

    public function destroy($id)
    {
        $flight = Flight::findOrFail($id);
        $flight->forceDelete();

        return redirect('/admin/flights');
    }
}
