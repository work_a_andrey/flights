<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminLoginController extends Controller
{
    public function index()
    {
        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
            return logged_user()->isAdmin()
                ? redirect()->to('/admin/')
                : redirect()->to('/');
        } else {
            return redirect()->back()->withErrors(['auth' => 'Wrong email or password']);
        }
    }
}