<?php

namespace App\Http\Controllers;

use App\Model\Asset;
use App\Http\Requests;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Response;

class AssetController extends Controller
{
    /**
     * @param $id
     * @return mixed
     */
    public function view($id)
    {
        if(!$asset = Asset::find($id)) {
            abort(404);
        }

        $path = base_path() . config('app.upload_folder') . $asset->path;

        $detect = function($filename='') {
            $filename = escapeshellcmd($filename);
            $command = "file -b --mime-type -m /usr/share/misc/magic {$filename}";

            $mimeType = shell_exec($command);

            return trim($mimeType);
        };

        $file = \File::get($path);
        $type = $detect($path);

        return response($file, 200,[
            "Content-Type" => $type
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function delete($id)
    {
        if(!logged_user()->isAdmin()) {
            return abort(403, 'Access forbidden');
        }
        if(!$asset = Asset::find($id)) {
            abort(404);
        }
        $asset->forceDelete();
        return back();
    }
}
