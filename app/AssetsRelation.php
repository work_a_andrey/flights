<?php

namespace App;

use App\Model\Asset;
use Illuminate\Support\Str;

trait AssetsRelation
{
    /**
     * @return mixed
     */
    public function assets()
    {
        return $this
            ->hasMany('App\Model\Asset', 'parent_id', 'id')
            ->where('parent_id', '=', $this->id)
            ->where('parent_type', '=', Str::lower((new \ReflectionClass($this))->getShortName()));
    }
    
    public function getMainAsset()
    {
        //TODO: implement main asset
        return null;
    }
}