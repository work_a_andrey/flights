<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aircrafts()
    {
        return $this->hasMany('App\Model\Aircraft', 'airlineId', 'id');
    }
}
