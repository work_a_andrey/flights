<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $fillable = ['name', 'airlineId'];
    
    public function airline()
    {
        return $this->belongsTo('App\Model\Airline', 'airlineId', 'id');
    }
}
