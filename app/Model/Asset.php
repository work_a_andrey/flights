<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'assets';

	protected static function boot()
	{
		parent::boot();

		static::deleting(function($model) {
			$path = base_path() . config('app.upload_folder') . $model->path;
			\File::delete($path);
		});
	}

	/**
	 * @return string
	 */
	public function getViewUrl()
	{
		return "/asset/" . $this->id;
	}

	/**
	 * @return string
	 */
	public function getDeleteUrl()
	{
		return '/asset/' . $this->id . '/delete';
	}
}
