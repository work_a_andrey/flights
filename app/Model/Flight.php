<?php

namespace App\Model;

use App\AssetsRelation;
use Illuminate\Database\Eloquent\Model;
use App\AssetsSaver;
use App\Model\Airline;
use App\Model\Aircraft;

class Flight extends Model
{
    use AssetsSaver;
    use AssetsRelation;
    
    protected $fillable = ['name', 'description', 'originId', 'destinationId', 'airlineId', 'aircraftId', 'departsOn',
    'arrivesOn', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function airline()
    {
        return $this->belongsTo('App\Model\Airline', 'airlineId', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aircraft()
    {
        return $this->belongsTo('App\Model\Aircraft', 'aircraftId', 'id');
    }

    public function origin()
    {
        return $this->belongsTo('App\Model\Airport', 'originId', 'id');
    }
    
    public function destination()
    {
        return $this->belongsTo('App\Model\Airport', 'destinationId', 'id');
    }
    
    /**
     * @return mixed|string
     */
    public function shortDescription()
    {
        return strlen($this->description) <= 150
            ? $this->description
            : substr($this->description, 0, 150) . '...';
    }
}
