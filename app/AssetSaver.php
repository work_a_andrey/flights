<?php 

namespace App;

use App\Model\Asset;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

trait AssetsSaver
{
    protected static function boot()
    {
        parent::boot();

        static::saved(function($model) {
            $newPath = config('app.upload_folder');
            if(!empty(Input::file('files'))) {
                foreach(Input::file('files') as $file) {
                    if(!$file instanceof UploadedFile) {
                        continue;
                    }
                    /** @var $file UploadedFile */
                    $newFilename = md5($file->getClientOriginalName()) . md5(microtime());
                    $asset = new Asset();
                    $asset->parent_type = Str::lower((new \ReflectionClass($model))->getShortName());
                    $asset->parent_id = $model->id;
                    $asset->path = $newFilename;
                    $asset->file_name = $file->getClientOriginalName();
                    $asset->file_type = $file->getClientOriginalExtension();
                    $asset->created_at = date('Y-m-d H:i:s');
                    $asset->updated_at = date('Y-m-d H:i:s');
                    $asset->save();
                    $file->move(base_path() . '/' . $newPath, $newFilename);
                }
            }
        });
    }
}