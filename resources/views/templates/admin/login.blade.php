@extends('layouts.basic')
@section('content')

    <div style="height: 50px;"></div>
    <h1 class="text-center login-title">@lang('admin-general.signin_to_continue')</h1>
    <div class="account-wall">
        <form class="form-signin" action="" method="post">
            @if($errors->any())
                <ul class="alert alert-danger" style="list-style-type: none">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            <input name="email" type="text" class="form-control" placeholder="Email" required autofocus>
            <input name="password" type="password" class="form-control" placeholder="@lang('admin-general.password')" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">
                @lang('admin-general.sign_in')
            </button>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>

@stop