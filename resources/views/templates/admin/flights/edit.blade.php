@extends('layouts.admin')
@section('content')
    <h1 class="page-header">@lang('admin-general.update')</h1>

    <div class="row">
        <div class="col-lg-12">
            <form action="/admin/flights/{{$flight->id}}" method="POST" enctype="multipart/form-data">
                {{method_field('PUT')}}
                @include('templates.admin.flights._form')
            </form>
        </div>
    </div>
@stop
