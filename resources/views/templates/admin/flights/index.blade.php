@extends('layouts.admin')
@section('content')
    <h1 class="page-header">@lang('admin-sidebar.flights')</h1>

    <p><a href="/admin/flights/create" class="btn btn-primary">@lang('admin-general.add_btn')</a></p>

    {!! $flights->links() !!}

    <!-- Table -->
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('admin-general.name')</th>
            <th>@lang('admin-general.airline')</th>
            <th>@lang('admin-general.aircraft')</th>
            <th>@lang('admin-general.origin')</th>
            <th>@lang('admin-general.destination')</th>
            <th>@lang('admin-general.departs_on')</th>
            <th>@lang('admin-general.arrives_on')</th>
            <th style="width: 15%;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($flights as $flight)
            <tr>
                <td>{{$flight->id}}</td>
                <td>
                    <a href="/admin/flights/{{$flight->id}}/edit">{{$flight->name}}</a>
                </td>
                <td>{{$flight->airline->name}}</td>
                <td>{{$flight->aircraft->name}}</td>
                <td>{{$flight->origin->name}}</td>
                <td>{{$flight->destination->name}}</td>
                <td>{{$flight->departsOn}}</td>
                <td>{{$flight->arrivesOn}}</td>
                <td style="text-align: right">
                    <form action="/admin/flights/{{$flight->id}}" method="POST">
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger btn-mini" onclick="if(!confirm('@lang("admin-general.delete_confirm")')) { return false;}">
                            @lang('admin-general.delete')
                        </button>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $flights->links() !!}
@stop
