@extends('layouts.admin')
@section('content')
    <h1 class="page-header">@lang('admin-general.create')</h1>

    <div class="row">
        <div class="col-lg-12">
            <form action="/admin/flights" method="POST" enctype="multipart/form-data">
                @include('templates.admin.flights._form')
            </form>
        </div>
    </div>
@stop
