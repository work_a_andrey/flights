<div class="form-group">
    <label for="account_id">@lang('admin-general.name')</label>
    <input type="text" class="form-control" name="flight[name]" value="{{$flight->name}}" id="" required>
</div>
<div class="form-group">
    <label for="account_id">@lang('admin-general.price')</label>
    <input type="number" class="form-control" name="flight[price]" value="{{$flight->price}}" id="" required>
</div>
<div class="form-group">
    <label for="account_id">@lang('admin-general.departs_on')</label>
    <input type="datetime" class="form-control" name="flight[departsOn]" value="{{$flight->departsOn}}" id="" required>
</div>
<div class="form-group">
    <label for="account_id">@lang('admin-general.arrives_on')</label>
    <input type="datetime" class="form-control" name="flight[arrivesOn]" value="{{$flight->arrivesOn}}" id="" required>
</div>
<div class="form-group">
    <label for="account_id">@lang('admin-general.origin')</label>
    <select name="flight[originId]" id="" class="form-control">
        @foreach(\App\Model\Airport::all() as $airport)
        <option value="{{$airport->id}}" @if($airport->id == $flight->originId) selected @endif>{{$airport->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="account_id">@lang('admin-general.destination')</label>
    <select name="flight[destinationId]" id="" class="form-control">
        @foreach(\App\Model\Airport::all() as $airport)
        <option value="{{$airport->id}}" @if($airport->id == $flight->destinationId) selected @endif>{{$airport->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="account_id">@lang('admin-general.aircraft')</label>
    <select name="flight[aircraftId]" id="" class="form-control">
        @foreach(\App\Model\Airline::all() as $airline)
            <optgroup label="{{$airline->name}}">
                @foreach($airline->aircrafts as $aircraft)
                    <option value="{{$aircraft->id}}" @if($aircraft->id == $flight->aircraftId) selected @endif>{{$aircraft->name}}</option>
                @endforeach
            </optgroup>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="description">@lang('admin-general.description')</label>
    <textarea name="flight[description]" id="description" cols="30" rows="10" class="visual-editor form-control">{{$flight->description}}</textarea>
</div>
<button type="submit" class="btn btn-primary">@lang('admin-general.save')</button>
<input type="hidden" name="_token" value="{{ csrf_token() }}">