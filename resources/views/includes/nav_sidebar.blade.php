<div class="secondary_nav_widgetized_area">
    <nav>
        <ul>
            <li><a href="index-gym.html">Gym                        </a></li>
            <li><a href="index-spa.html">Spa</a></li>
            <li><a href="index-package-offer.html">Package</a></li>
            <li><a href="room-single.html">Room Single </a></li>
            <li><a href="blog-boxed.html">Blog Masonry</a></li>
            <li><a href="room-list-compact.html">Room List Grid</a></li>
            <li><a href="single.html">Post Single </a></li>
            <li><a href="booking.html">Booking</a></li>
            <li><a href="404.html">404</a></li>
        </ul>
    </nav>
    <aside class="widget widget_text">
        <h4>ABOUT XENIA</h4>
        <div class="textwidget text-justify">   Either you look for a luxurious experience or a remarkable on the go experience, Xenia Resort is the place to visit and spend the time at. We offer quality services.</div>
        <div class="text_block">
            <div><i class="icon icon-Phone"></i>255 - 5546 989</div>
            <div><i class="icon icon-Flag"></i>Mercury Hills 40, Atlanta</div>
            <div><i class="icon icon-Keyboard"></i>support@xenia.com</div>
        </div>
    </aside>
    <div class="col-md-12">
        <div class="secondary_widgetized_area_copyright small_screen_text_center">
            <p>COPYRIGHT © 2015 ALL RIGHTS RESERVED</p>
        </div>
    </div>
</div>