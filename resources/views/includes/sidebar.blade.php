<ul class="nav nav-sidebar">
    <li @if(is_current_url('/'))class="active"@endif>
        <a href="/">Dashboard  @if(is_current_url('/admin/codes'))<span class="sr-only">(current)</span>@endif</a>
    </li>
    @if(logged_user() && logged_user()->isAdmin())
    <li @if(is_current_url('/admin/codes'))class="active"@endif>
        <a href="/admin/codes">Codes @if(is_current_url('/admin/codes'))<span class="sr-only">(current)</span>@endif</a>
    </li>
    <li @if(is_current_url('/admin/accounts'))class="active"@endif>
        <a href="/admin/accounts">Accounts @if(is_current_url('/admin/accounts'))<span class="sr-only">(current)</span>@endif</a>
    </li>
    @endif
</ul>
