<ul class="nav nav-sidebar">
    <li @if(is_current_url('/admin/flights'))class="active"@endif>
        <a href="/admin/flights">@lang('admin-sidebar.flights')  @if(is_current_url('/admin/flights'))<span class="sr-only">(current)</span>@endif</a>
    </li>
</ul>
