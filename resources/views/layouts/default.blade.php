<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
</head>
<body style="">
<div id="page_wrapper">
    @yield('content')
    @include('includes.footer')
    @include('includes.nav_sidebar')
</div>
@include('includes.scripts')
</body>
</html>