<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.admin.head')
</head>
<body>
@include('includes.admin.header')

<div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
        @include('includes.admin.sidebar')
    </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        @if (session('status'))
            <div class="alert alert-info">
                {{ session('status') }}
            </div>
        @endif
        @yield('content')
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.visual-editor').redactor({
            minHeight: 150
        });
    });
</script>
</body>
</html>