<div class="form-group">
    <label for="description">@lang('helpers.images')</label>
    <br>
    <input type="file" id="files" name="files[]" multiple/>
    <div class="object-images-container">
        @foreach($object->assets as $asset)
        <div class="object-image">
            <img src="{{$asset->getViewUrl()}}" alt="" style="height: 120px; width: 120px;">
            <br><a href="{{$asset->getDeleteUrl()}}" class="object-image-delete">@lang('helpers.delete')</a>
        </div>
        @endforeach
    </div>
</div>
<style>
    .object-add-image-trigger, .object-image-delete {
        border-bottom: 1px dashed #337ab7;
        text-decoration: none !important;
    }

    .object-images-container .object-image {
        display: inline-block;
        margin: 15px 15px 0 0;
        text-align: center;
    }
</style>

<script type="text/javascript">
    $(document).click(function() {
        $('.object-add-image-trigger').on('click', function() {
            $('#files').click();
        });
    });
    function handleFileSelect(evt) {
        var files = evt.target.files;
        for (var i = 0, f; f = files[i]; i++) {
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    var div = $('<div class="object-image">' +
                            '<img src="'+e.target.result+'" alt="" style="height: 120px; width: 120px;"/>' +
                            '<br><span>&nbsp;</span></div>');
                    div.appendTo('.object-images-container')
                };
            })(f);

            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>