<?php

use Illuminate\Database\Seeder;
use \App\Model\Airline;

class AirlinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $airlines = ['ACM Air Charter', 'Aero-Dienst', 'AeroLogic', 'Air Berlin', 'Deutsche Zeppelin Reederei'];
        foreach($airlines as $airline) {
            Airline::create([
                'name' => $airline
            ]);
        }
    }
}
