<?php

use Illuminate\Database\Seeder;
use \App\Model\Airport;

class AirportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $airports = ['Aachen', 'Berlin', 'Dortmund', 'Emden', 'Hahn', 'Hannover', 'Leipzig', 'Stuttgart'];
        foreach ($airports as $airport) {
            Airport::create([
                'name' => $airport
            ]);
        }
    }
}
