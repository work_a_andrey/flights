<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Default',
            'is_admin' => 0
        ]);

        Role::create([
            'name' => 'Admin',
            'is_admin' => 1
        ]);
    }
}
