<?php

use Illuminate\Database\Seeder;
use App\Model\Airline;
use App\Model\Airport;
use App\Model\Flight;
use \Illuminate\Support\Str;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Collection;

class FlightsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $airports = Airport::all();

//        $flight = Flight::find(1);
//        dd($flight->airline);
        
        foreach ($airports as $origin) {
            foreach ($airports as $destination) {
                if ($origin->id == $destination->id) {
                    continue;
                }
                foreach (Airline::all() as $airline) {
                    $aircrafts = $airline->aircrafts;
                    foreach($aircrafts as $aircraft) {
                        try {
                            Flight::create([
                                'name' => Str::upper(Str::random(2)) . rand(1024, 9999),
                                'originId' => $origin->id,
                                'destinationId' => $destination->id,
                                'airlineId' => $airline->id,
                                'aircraftId' => $aircraft->id,
                                'departsOn' => Carbon::now()->addHours(rand(10, 20))->toDateTimeString(),
                                'arrivesOn' => Carbon::now()->addHours(rand(21, 40))->toDateTimeString(),
                                'price' => mt_rand (20, 500) + (mt_rand (20, 500) / 100)
                            ]);
                        } catch (\Exception $e) {
                            continue;
                        }

                    }
                }
            }
        }

    }
}
