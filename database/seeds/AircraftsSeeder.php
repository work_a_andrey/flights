<?php

use Illuminate\Database\Seeder;
use \App\Model\Airline;
use \App\Model\Aircraft;

class AircraftsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aircrafts = ['Airbus A318', 'Airbus A319', 'Airbus A320', 'Airbus A321', 'Airbus A380'];
        foreach(Airline::all() as $airline) {
            for($i = 0; $i < rand(1, count($aircrafts) - 1); $i++ ) {
                Aircraft::create([
                    'name' => $aircrafts[$i],
                    'airlineId' => $airline->id
                ]);
            }
        }
    }
}
