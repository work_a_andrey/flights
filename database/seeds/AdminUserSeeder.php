<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@email.com',
            'password' => \Illuminate\Support\Facades\Hash::make('password'),
            'role_id' => \App\Role::where('is_admin', '=', 1)->first()->id
        ]);
    }
}
