<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(AdminUserSeeder::class);
        $this->call(AirlinesSeeder::class);
        $this->call(AircraftsSeeder::class);
        $this->call(AirportsSeeder::class);
        $this->call(FlightsSeeder::class);
    }
}
