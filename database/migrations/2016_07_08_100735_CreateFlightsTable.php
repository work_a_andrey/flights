<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->dateTime('departsOn');
            $table->dateTime('arrivesOn');
            $table->double('price');
            $table->text('description');
            $table->integer('originId');
            $table->integer('destinationId');
            $table->integer('airlineId');
            $table->integer('aircraftId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
